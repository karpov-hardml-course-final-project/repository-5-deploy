# Репозиторий 5. Разворачивание решения

[Телеграмм для связи: Андрей Полетаев](https://t.me/andrey_poletaev)

В этом репозитории содержатся скрипты и конфигурационные файлы, необходимые для разворачивания приложения. Предполагается, что большая часть будет выполняться с помощью gitlab ci/cd скриптов.

## Этапы разворачивания

- [x] скопировать файлы на каждый хост (ansible)
- [x] поднять эмбеддинг-сервер (описание в [отдельном репозитории](https://gitlab.com/karpov-hardml-course-final-project/repository-2-data-science))
- [x] поднять knn-worker приложение
- [x] поднять API gateway приложение


## Запуск команд для разворачивания

### Копирование файлов из S3 и создание симлинков:

[Полезная статья](https://www.middlewareinventory.com/blog/ansible-aws_s3-example/#Ansible_S3_List_Objects_in_a_Bucket)

Конфигурация (из какого s3 бакета брать данные и стадия: blue или green) задаётся в файлах конфигурации `config/gen1_green.json` и `config/gen2_blue.json`

```
ansible-playbook ansible_playbooks/0_copy_from_s3.yml -i config/ansible.cfg.yml --extra-vars "@config/gen1_green.json"
```

`0_copy_from_s3.yml` установит pip3, потом клиент s3 (boto), подключится к s3, скачает файлы в заданную директорию и создать симлинки.

### Создание Swarm сервиса

Конфигурация для каждой стадии blue/green задаётся в тех же файлах конфигурации `config/gen1_green.json` и `config/gen2_blue.json`

```
ansible-playbook ansible_playbooks/1_service_create.yml -i config/ansible.cfg.yml --extra-vars "@config/gen1_green.json"
```


## Черновики

### Копирование файлов

```
ansible-playbook ansible_playbooks/1_copy_file.yml -i config/ansible.cfg.yml --extra-vars "dir=PATH"
```

где `PATH` –– абсолютный путь к директории.